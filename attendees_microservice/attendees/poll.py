import json
import requests

from .models import ConferenceVO


def get_conferences():
    # monolith(is connected from settings.py when wer added it to allowed hosts)
    url = "http://monolith:8000/api/conferences/"
    # sending a get request to the url below/above
    response = requests.get(url)
    # turns json into a python string to equal to content
    content = json.loads(response.content)
    print(content)
    for conference in content["conferences"]:
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
