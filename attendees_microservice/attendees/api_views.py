from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AccountVO

# from events.api_views import ConferenceListEncoder
from .models import Attendee
from .models import ConferenceVO
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",  # due to foreign key have to use encoders extra line
    ]
    encoders = {"conference": ConferenceVODetailEncoder()}


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",  # due to foreign key have to use encoders extra line
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder()
    }  # which one is correct?

    def get_extra_data(self, o):
        # get the count of AccountVo objects with email equal to o.email
        count = AccountVO.objects.filter(email=o.email).count()
        # return a dictionary  with "has account": True if count >0
        if count > 0:
            print(count)
            return {"has_account": True}
        # Otherwise, return a dictionary with "has_account": False
        else:
            print(count)
            return {"has_account": False}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    """# response = []
    # attendees = Attendee.objects.all()
    # for atendee in attendees:
    #     response.append(
    #         {
    #             "name": atendee.name,
    #             "href": atendee.get_api_url(),
    #         }
    #     )

    # return JsonResponse({"attendees": response})

    # list comprehension
    # attendees = [
    #     {
    #         "name": atendee.name,
    #         "href": atendee.get_api_url(),
    #     }
    #     for atendee in Attendee.objects.all()
    # ]
    # return JsonResponse({"attendees": attendees})"""
    if request.method == "GET":
        attendee = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendee": attendee}, encoder=AttendeeListEncoder
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"

            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, pk):
    """
    Returns the details for the Attendee model specified
    by the pk parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    """# attendee = Attendee.objects.get(id=pk)
    # return JsonResponse(
    #     {
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url(),
    #         },
    #     }
    # )

    # List comprehension
    # attendee = [{
    #         "email": attendee.email,
    #         "name": attendee.name,
    #         "company_name": attendee.company_name,
    #         "created": attendee.created,
    #         "conference": {
    #             "name": attendee.conference.name,
    #             "href": attendee.conference.get_api_url()}
    #     for atendee in Attendee.objects.get(id=pk)
    # ]
    # return JsonResponse({"attendees": attendees})"""
    if request.method == "GET":
        attendee = Attendee.objects.get(id=pk)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    content = json.loads(request.body)
    # new code
    Attendee.objects.filter(id=pk).update(**content)

    # copied from detail
    attendee = Attendee.objects.get(id=pk)
    return JsonResponse(attendee, encoder=AttendeeDetailEncoder, safe=False)
