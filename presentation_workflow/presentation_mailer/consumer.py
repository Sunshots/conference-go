import json
import pika
import django
import os
import sys
import time
from pika.exceptions import AMQPConnectionError
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approvals(ch, method, properties, body):
    print("  Received %r" % body)
    # body comes from producer function. the producer sent the message/body
    # as a string, we use json.loads(to change body/string to python dict)
    # to read the body
    content = json.loads(body)
    # set our variable to content.
    # to locate the correct value
    # use brackets[locate particular key in the python dict]
    send_mail(
        "Your Presentaion has been accepted",
        f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been accepted",
        "admin@conference.go",
        [f"{content['presenter_email']}"],
        fail_silently=False,
    )


# do the same thing from approvals to make it for rejection
def process_rejections(ch, method, properties, body):
    print("  Received %r" % body)
    content = json.loads(body)
    send_mail(
        "Your Presentaion has been rejected",
        f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been rejected",
        "admin@conference.go",
        [f"{content['presenter_email']}"],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        # you have to have both a queue name for approval and rejections
        # since our views has created both queues
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        # Configure the consumer to call the process_approval/rejections function
        # when a message arrives
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approvals,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejections,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
